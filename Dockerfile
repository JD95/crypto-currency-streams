FROM alpine:latest

RUN apk add --no-cache libgcc openssl-dev

WORKDIR /app
COPY target /app

EXPOSE 4040

CMD release/onyx
