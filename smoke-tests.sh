sleep 5

HOST='localhost'
ORIGIN='localhost'
URL='docker_dind:4040/test?exchange=binance&symbols=ADA|ETH'

C='Connection: Upgrade'
U='Upgrade: websocket'
H="Host: $HOST"
O="Origin: $ORIGIN"
P='Sec-WebSocket-Protocol: websocket'
K='Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ=='

echo "should receive ping from application..."
if curl -i -N -m 2 -H "$C" -H "$U" -H "$H" -H "$O" -H "$P" -H "$K" "$URL" | grep -q -e "Switching Protocols"; then
  printf "\n- application loaded successfully\n"
  exit 0
else
  printf "\n- application failed to load\n"
  exit 1
fi
