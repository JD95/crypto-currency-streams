
var exchanges = {
    binance: ["ADA|BTC", "ADA|ETH", "ADX|BNB"],
    gdax: ["BCH|BTC", "BTC|USD", "ETH|EUR"],
    bitfinex: ["AGI|BTC", "IOTA|BTC", "RDN|USD"]
};

var sockets = [];

function test() {
    var i;
    for(i=0; i<100; i++){
        var n = Math.floor(Math.random() * 100) % 3;
        console.log(n);
        var exchange = ["binance", "gdax", "bitfinex"][n]; 
        var symbols = [exchanges.binance, exchanges.gdax, exchanges.bitfinex][n];
        var m = Math.floor(Math.random() * 100) % symbols.length; 
        var symbol = symbols[m];
        var conn = "ws://127.0.0.1:4040/test?exchange=" + exchange + "&symbols=" + symbol;
        var exampleSocket = new WebSocket(conn, "websocket");
        exampleSocket.onmessage = function(event) {
            var p = document.createElement("P");
            p.textContent = event.data;
            document.getElementById("data").appendChild(p);
        };
        console.log(conn);
        sockets.push(exampleSocket);
    }
}

test();
