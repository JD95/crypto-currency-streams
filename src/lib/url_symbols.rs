pub fn replace_pipe(s: &String) -> String {
    s.chars().map(|c| if c == '|' { '-' } else { c }).collect()
}

pub fn filter_pipe(s: &String) -> String {
    s.chars().filter(|c| !c.eq(&'|')).collect::<String>()
}

pub fn symbol_lowercase(s: &String) -> String {
    filter_pipe(s).to_lowercase()
}
