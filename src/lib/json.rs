use serde_json::Value;
use serde;
use serde::ser::{SerializeStruct, Serialize, Serializer};
use std::str::FromStr;

#[derive(Clone)]
pub enum Side {
    Buy,
    Sell,
}

impl Side {
    pub fn try_from_str(s: String) -> Option<Side> {
        match s.as_str() {
            "buy" => Some(Side::Buy),
            "sell" => Some(Side::Sell),
            _ => None
        }
    }

    pub fn try_from_bool(b: bool) -> Option<Side> {
        Some(if b { Side::Buy } else { Side::Sell })
    }

    pub fn try_from_f64(n: f64) -> Option<Side> {
        Some(if n >= 0.0 { Side::Buy } else { Side::Sell })
    }
}

impl Serialize for Side {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            &Side::Buy => serializer.serialize_i32(1),
            &Side::Sell => serializer.serialize_i32(0),
        }
    }
}

#[derive(Serialize, Clone)]
pub struct TradeData {
    pub id: String,
    pub timestamp: i64,
    pub datetime: String,
    pub side: Side,
    pub price: f64,
    pub amount: f64,
}

#[derive(Serialize, Clone)]
pub struct DepthPoint {
    pub p: f64,
    pub a: f64,
}

impl DepthPoint {
    pub fn new(p: f64, a: f64) -> DepthPoint {
        DepthPoint { p: p, a: a }
    }

    pub fn from_strings(p: &String, a: &String) -> Option<DepthPoint> {
        strict_match!(
            Ok(price) = f64::from_str(p.as_str()),
            Ok(amount) = f64::from_str(a.as_str()), {
                DepthPoint::new(price, amount)
            }
        )

    }
}

#[derive(Serialize)]
pub struct DepthData {
    pub asks: Vec<DepthPoint>,
    pub bids: Vec<DepthPoint>,
}

impl DepthData {
    pub fn new(asks: Vec<DepthPoint>, bids: Vec<DepthPoint>) -> DepthData {
        DepthData { asks: asks, bids: bids }
    }
}

pub struct UnifiedFormat {
    pub exchange: String,
    pub symbol: String,
    pub trades: Vec<TradeData>,
    pub depth: Option<DepthData>,
}

impl Serialize for UnifiedFormat {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        use std::collections::HashMap;

        let mut format = ser.serialize_struct("UnifiedFormat", 4)?;
        format.serialize_field("exchange", &self.exchange)?;
        format.serialize_field("symbol", &self.symbol)?;
        format.serialize_field("trades", &self.trades)?;
        match &self.depth {
            &Some(ref d) => format.serialize_field("depth5", d)?,
            _ => {
                let hm: HashMap<String, ()> = HashMap::new();
                format.serialize_field("depth5", &hm)?
            }
        }
        format.end()
    }
}

pub fn json_str_cast<T: FromStr + serde::Serialize>(obj: &Value) -> Option<T> {
    use serde_json::Value::*;
    match obj {
        &String(ref s) => {
            match T::from_str(s) {
                Ok(n) => Some(n),
                _ => None,
            }
        }
        _ => None,
    }
}

pub fn json_side(obj: &Value) -> Option<Side> {
    use serde_json::Value::*;
    let side_nouns = vec!["true", "buy"];
    match obj {
        &String(ref s) => Some(if side_nouns.contains(&s.as_str()) {
            Side::Buy
        } else {
            Side::Sell
        }),
        &Bool(ref b) => Some(if *b { Side::Buy } else { Side::Sell }),
        &Number(ref n) => match n.as_f64() {
            Some(n) => Some(if n >= 0.0 { Side::Buy } else { Side::Sell }),
            _ => None,
        }
        _ => None,
    }
}

pub fn norm_depth<F>(obj: &Value, f: &F) -> Option<Vec<DepthPoint>>
where
    F: Fn(&Vec<Value>) -> (Value, Value),
{
    use serde_json::Value::*;

    match obj {
        &Array(ref xs) => {
            xs.iter()
                .map(|x| match x {
                    &Array(ref xs) => {
                        let (p_value, a_value) = f(xs);
                        strict_match!(
                            Some(p) = json_str_cast::<f64>(&p_value),
                            Some(a) = json_str_cast::<f64>(&a_value),  {
                                DepthPoint{p: p, a: a}
                            })
                    }
                    _ => None,
                })
                .collect()
        }
        _ => None, 
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::*;

    #[test]
    fn proper_serialize_empty() {

        let test = UnifiedFormat {
            exchange: "binance".to_string(),
            symbol: "ADA|BTC".to_string(),
            trades: Vec::new(),
            depth: None,
        };

        let target = json!({
            "exchange": "binance",
            "symbol": "ADA|BTC",
            "trades": [],
            "depth5": {}
        });

        assert_eq!(to_value(test).unwrap(), target);
    }

    #[test]
    fn proper_serialize_side() {
        assert_eq!(to_value(Side::Buy).unwrap(), json!(1));
        assert_eq!(to_value(Side::Sell).unwrap(), json!(0));
    }

    #[test]
    fn proper_serialize_trades() {

        let trade = TradeData {
            id: "1234".to_string(),
            timestamp: 1234,
            datetime: "1234".to_string(),
            side: Side::Buy,
            price: 1234.5,
            amount: 1.2,
        };

        let test = UnifiedFormat {
            exchange: "binance".to_string(),
            symbol: "ADA|BTC".to_string(),
            trades: vec![trade],
            depth: None,
        };

        let target = json!({
            "exchange": "binance",
            "symbol": "ADA|BTC",
            "trades": [
                { "id": "1234",
                   "timestamp": 1234,
                   "datetime": "1234",
                   "side": 1,
                   "price": 1234.5,
                   "amount": 1.2
                }],
            "depth5": {}
        });

        assert_eq!(to_value(test).unwrap(), target);
    }

    #[test]
    fn proper_serialize_depth() {

        let depth = DepthData {
            asks: vec![DepthPoint{ p: 1234.5, a: 1234.5}],
            bids: vec![DepthPoint{ p: 1234.5, a: 1234.5}],
        };

        let test = UnifiedFormat {
            exchange: "binance".to_string(),
            symbol: "ADA|BTC".to_string(),
            trades: Vec::new(),
            depth: Some(depth),
        };

        let target = json!({
            "exchange": "binance",
            "symbol": "ADA|BTC",
            "trades": [],
            "depth5": {
                "asks": [{"p": 1234.5, "a": 1234.5}],
                "bids": [{"p": 1234.5, "a": 1234.5}]
            }
        });

        assert_eq!(to_value(test).unwrap(), target);
    }
}
