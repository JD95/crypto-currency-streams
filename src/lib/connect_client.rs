use hyper::uri::RequestUri::*;
use slog;
use std::sync::RwLockReadGuard;
use url::Url;
use websocket::server::upgrade::WsUpgrade;
use websocket::server::upgrade::sync::Buffer;
use websocket::stream::sync::TcpStream;

use exchange::{Exchange, Exchanges};

fn parse_url_params(url: String) -> Result<(String, String), &'static str> {
  Url::parse(["ws://site", url.as_str()].concat().as_str())
    .map_err(|_| "malformed endpoint")
    .and_then(|url| {
      let mut pairs = url.query_pairs();
      let some_exchange = pairs.find(|p| p.0 == "exchange").map(|p| p.1.into_owned());
      let some_symbol = pairs.find(|p| p.0 == "symbols").map(|p| p.1.into_owned());
      some_exchange
        .and_then(|exchange| some_symbol.map(|symbol| Ok((exchange, symbol))))
        .unwrap_or(Err("exchange and symbols not found"))
    })
}

fn accept_connection(
  log: &slog::Logger,
  symbol: String,
  upgrade: WsUpgrade<TcpStream, Option<Buffer>>,
  stream: &Box<Exchange + Send + Sync>,
) -> Result<(), String> {
  upgrade
    .use_protocol("websocket")
    .accept()
    .map_err(|(_, io_err)| io_err.to_string())
    .and_then(move |client| {
      use std::time::Duration;
      {
        let client_stream = client.stream_ref();
        let _ = client_stream.set_read_timeout(Some(Duration::new(0, 150)));
        let _ = client_stream.set_write_timeout(Some(Duration::new(0, 150)));
      }
      stream.subscribe(log, client, symbol)
    })
}

pub fn connect_client(
  log: &slog::Logger,
  upgrade: WsUpgrade<TcpStream, Option<Buffer>>,
  exchanges: RwLockReadGuard<Exchanges>,
) {
  if let AbsolutePath(p) = upgrade.request.subject.1.clone() {
    let parse_result = parse_url_params(p);
    match parse_result {
      Ok((exchange, symbol)) => {
        let _ = (*exchanges)
          .get(&exchange)
          .ok_or(
            "client attempted to connect to non-existant exchange ".to_string() + exchange.as_str(),
          )
          .and_then(|api_stream| {
            accept_connection(log, symbol, upgrade, api_stream)
          })
          .map_err(|e| {
            error!(log, "client-connection";
                             "error" => "could not connect client",
                             "detail" => format!("{}", e))
          });
      }
      Err(_e) => {
        upgrade.drop();
      }
    }
  }
}
