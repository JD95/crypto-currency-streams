use native_tls::TlsConnector;
use slog::Logger;
use std::sync::mpsc::Sender;
use std::time::Duration;
use websocket::client::ClientBuilder;
use websocket::client::sync::Client;
use websocket::message::OwnedMessage;
use websocket::stream::sync::{TcpStream, TlsStream};

use api_stream::ApiStream;
use exchange::{Exchange, Exchanges};
use stream_types::{Connection, ConnectionCmd, RestartingStream, ServerEvent, Symbols};
use util::Named;

pub fn setup_connection(connection: Connection) -> Result<Client<TlsStream<TcpStream>>, String> {
  let connector = TlsConnector::builder().unwrap().build().unwrap();
  ClientBuilder::new(connection.uri.as_str())
    .map_err(|e| e.to_string())
    .and_then(|mut c| {
      c.connect_secure(Some(connector))
        .map_err(|e| e.to_string())
        .and_then(move |mut stream| {
          for cmd in connection.instructions {
            match cmd {
              ConnectionCmd::Send(m) => {
                let _ = stream.send_message(&OwnedMessage::Text(m));
              }
            }
          }

          {
            let stream = stream.stream_ref().get_ref();
            let _ = stream.set_read_timeout(Some(Duration::new(1, 0)));
            let _ = stream.set_write_timeout(Some(Duration::new(1, 0)));
          }

          Ok(stream)
        })
    })
}

pub fn launch_data_stream<T>(
  log: Logger,
  exchanges: &mut Exchanges,
  push_data: Sender<ServerEvent>,
) -> Vec<RestartingStream>
where
  T: Exchange + ApiStream + Named + Sync + Send + 'static,
{
  let exchange = T::new();
  let name = T::name();
  let mut streams = Vec::new();

  {
    let symbols: &Symbols = exchange.extract();

    for connection in T::mk_connection(&symbols.0) {
      streams.push(RestartingStream::new(
        name.to_string(),
        log.clone(),
        connection.clone(),
        push_data.clone(),
      ));
    }
  }

  exchanges.insert(name.to_string(), Box::new(exchange));
  streams
}
