use reqwest;
use serde_json;
use serde_json::Value;
use serde_json::map::Map;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use websocket::stream::sync::TcpStream;
use websocket::sync::Client;

use api::Api;
use api_stream::{ApiStream, FetchSymbols};
use json::*;
use stream_types::{Connection, ConnectionCmd, SymbolPair, Symbols};
use url_symbols;
use util::{Named, New, Tagged};

const BITFINEX_SYMBOLS_API: &'static str = "https://api.bitfinex.com/v1/symbols";
const BITFINEX_WS: &'static str = "wss://api.bitfinex.com/ws";
const EXCHANGE: &'static str = "bitfinex";

pub struct Bitfinex;

impl Named for Bitfinex {
  fn name() -> &'static str {
    EXCHANGE
  }
}

#[derive(Debug, Eq, PartialEq)]
pub enum BitfinexChannel {
  Trade(SymbolPair),
  Book(SymbolPair),
}

use self::BitfinexChannel::*;

pub struct Tick;
pub struct Depth;

pub enum BitfinexData {
  Trade(Vec<Value>),
  Depth(Vec<Value>),
}

fn stream_requests(symbols: &Vec<String>) -> Vec<ConnectionCmd> {
  let mut trades: Vec<ConnectionCmd> = symbols
    .iter()
    .map(|x: &String| x.chars().collect::<String>().to_uppercase())
    .map(|x: String| {
      json!({
                "event": "subscribe",
                "channel": "trades",
                "pair": x, 
            }).to_string()
    })
    .map(|m| ConnectionCmd::Send(m))
    .collect();

  let books: Vec<_> = symbols
    .iter()
    .map(|x: &String| x.chars().collect::<String>().to_uppercase())
    .map(|x: String| {
      json!({
                "event": "subscribe",
                "channel": "book",
                "pair": x,
                "prec": "P0",
                "length": "25",
                "frequencey": "F0"
            }).to_string()
    })
    .map(|m| ConnectionCmd::Send(m))
    .collect();

  trades.extend(books);
  trades
}

fn expand_depth(price: f64, amount: f64) -> DepthData {
  if amount >= 0.0 {
    DepthData {
      asks: Vec::new(),
      bids: vec![DepthPoint{ p: price, a: amount }],
    }
  } else {
    DepthData {
      asks: vec![DepthPoint{ p: price, a: amount.abs() }],
      bids: Vec::new(),
    }
  }
}

fn parse_payload_subscription(subscription: Map<String, Value>) -> Option<(i64, BitfinexChannel)> {
  use serde_json::Value::*;

  let has_keys = vec!["pair", "channel", "chanId"].iter().all(|x| {
    subscription.contains_key(&x.to_string())
  });
  if has_keys {
    let fields = strict_match!(
            Some(v_pair) = subscription.get("pair"),
            Some(v_channel) = subscription.get("channel"),
            Some(v_channel_id) = subscription.get("chanId"), {
                (v_pair, v_channel, v_channel_id)
            });
    fields
      .and_then(|fs| {
        strict_match!(
                    &String(ref pair) = fs.0,
                    &String(ref channel_name) = fs.1,
                    Some(channel_id) = fs.2.as_i64(), {
                        let symbol = SymbolPair::new(pair.to_string().to_lowercase());
                        if channel_name.eq("trades") {
                            Some((channel_id, Trade(symbol)))
                        } else if channel_name.eq("book") {
                            Some((channel_id, Book(symbol)))
                        }
                        else { None }
                    })
      })
      .and_then(|result| result)
  } else {
    None
  }
}


pub struct LookupChannel {
  lookup_channel: HashMap<i64, BitfinexChannel>,
}

impl New for LookupChannel {
  fn new() -> LookupChannel {
    LookupChannel { lookup_channel: HashMap::new() }
  }
}

impl LookupChannel {
  fn parse_payload_subscription(
    &mut self,
    subscription: Map<String, Value>,
  ) -> Vec<(SymbolPair, BitfinexData)> {

    if let Some((id, channel)) = parse_payload_subscription(subscription) {
      let _ = self.lookup_channel.insert(id, channel);
    }
    vec![]
  }

  fn parse_payload_trade(&self, trade: Vec<Value>) -> Vec<(SymbolPair, BitfinexData)> {
    if let Some(id) = trade.get(0).and_then(|t| t.as_i64()) {
      self
        .lookup_channel
        .get(&id)
        .map(|ch| match ch {
          &Trade(ref p) => vec![(p.clone(), BitfinexData::Trade(trade))],
          &Book(ref p) => vec![(p.clone(), BitfinexData::Depth(trade))],
        })
        .unwrap_or(Vec::new())

    } else {
      Vec::new()
    }
  }
}

fn unify_trade(payload: &Vec<Value>) -> Option<UnifiedFormat> {
  use serde_json::Value::*;
  let indices = match payload.len() {
    6 => Some((2, 3, 5, 4, 5)),
    7 => Some((2, 4, 6, 5, 6)),
    _ => None,
  };
  indices.and_then(|idcs| {
    let fields = strict_match!(
            Some(v_id) = payload.get(idcs.0),
            Some(v_timestamp) = payload.get(idcs.1),
            Some(v_side) = payload.get(idcs.2),
            Some(v_price) = payload.get(idcs.3),
            Some(v_amount) = payload.get(idcs.4), {
                (v_id, v_timestamp, v_side, v_price, v_amount)
            });
    fields.and_then(|fs| {
      strict_match!(
            String(id) = fs.0.clone(),
            Some(timestamp) = fs.1.as_i64(),
            Some(side) = json_side(&fs.2),
            Some(price) = fs.3.as_f64(),
            Some(amount) = fs.4.as_f64(), {
                let trade = TradeData{
                    id: id.to_string(),
                    timestamp: timestamp,
                    datetime: "".to_string(),
                    side: side,
                    price: price,
                    amount: amount.abs()
                };
                UnifiedFormat {
                    exchange: EXCHANGE.to_string(),
                    symbol: "".to_string(),
                    trades: vec![trade],
                    depth: None
                }
            })
    })
  })
}

fn unify_book(depth: &Vec<Value>) -> Option<UnifiedFormat> {
  let depths = strict_match!(
        Some(price) = depth.get(1).and_then(|p| p.as_f64()),
        Some(amount) = depth.get(3).and_then(|p| p.as_f64()), {
            expand_depth(price, amount)
        });
  depths.map(|ds| {
    UnifiedFormat {
      exchange: EXCHANGE.to_string(),
      symbol: "".to_string(),
      trades: Vec::new(),
      depth: Some(ds),
    }
  })
}

impl FetchSymbols for Bitfinex {
  fn fetch_symbols() -> Symbols {
    let exchange_info = reqwest::get(BITFINEX_SYMBOLS_API).unwrap().text().unwrap();
    let info_json: Result<Vec<String>, serde_json::Error> = serde_json::from_str(&exchange_info);
    Symbols(info_json.unwrap_or(vec![]))
  }
}

pub type BitfinexStream = Tagged<Bitfinex, Api<Client<TcpStream>, Arc<Mutex<LookupChannel>>>>;

impl ApiStream for BitfinexStream {
  type Payload = BitfinexData;
  fn format_symbols(s: &String) -> SymbolPair {
    SymbolPair::new(url_symbols::symbol_lowercase(s))
  }

  fn mk_connection(symbols: &Vec<String>) -> Vec<Connection> {
    let requests = stream_requests(symbols);
    vec![ Connection {
            uri: BITFINEX_WS.to_string(),
            lifetime: Duration::new(10800,0),
            delay: Duration::new(30,0),
            instructions: requests
        }]
  }

  fn parse_payload(&self, s: String) -> Vec<(SymbolPair, Self::Payload)> {
    use serde_json::Value::*;

    let parse: Result<Value, serde_json::Error> = serde_json::from_str(&s);
    parse
      .map(|p| match p {
        Object(subscription) => {
          if let Ok(mut lock) = self.value.extension.lock() {
            (*lock).parse_payload_subscription(subscription)
          } else {
            Vec::new()
          }
        }
        Array(vs) => {
          if let Ok(lock) = self.value.extension.lock() {
            (*lock).parse_payload_trade(vs)

          } else {
            Vec::new()
          }
        }
        _ => Vec::new(), 
      })
      .unwrap_or(Vec::new())
  }

  fn unify(payload: Self::Payload) -> Option<UnifiedFormat> {
    match payload {
      BitfinexData::Trade(trade) => unify_trade(&trade), 
      BitfinexData::Depth(depth) => unify_book(&depth),
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use serde_json::from_reader;
  use std::fs::File;

  fn with_mock_data<T>(test: T)
  where
    T: FnOnce(Value),
  {
    match File::open("assets/mock.json") {
      Ok(file) => {
        match from_reader(file) {
          Ok(data) => test(data),
          Err(e) => panic!("error parsing mock.json: {}", e),
        }
      }
      Err(e) => panic!("error opening mock.json: {}", e),
    }
  }

  #[test]
  fn bitfinex_parse_subscription_payload() {
    with_mock_data(|data| {
      let payload = data["bitfinex_subscription"].as_object().unwrap();
      assert!(parse_payload_subscription(payload.clone()).is_some())
    })
  }

  #[test]
  fn bitfinex_unify_book() {
    with_mock_data(|data| {
      let payload = &data["bitfinex_book"].as_array().unwrap();
      assert!(unify_book(payload.clone()).is_some())
    })
  }

  #[test]
  fn bitfinex_unify_trade() {
    with_mock_data(|data| {
      let payload = data["bitfinex_trades"].as_array().unwrap();
      for trade in payload {
        assert!(unify_trade(trade.as_array().unwrap()).is_some())
      }
    })
  }

}
