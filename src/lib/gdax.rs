use reqwest;
use serde_json::Value;
use serde_json;
use std::time::Duration;
use std::str::FromStr;
use websocket::sync::Client;
use websocket::stream::sync::TcpStream;

use api::Api;
use json::*;
use url_symbols;
use api_stream::{ApiStream, FetchSymbols};
use util::{Named, Tagged};
use stream_types::{Connection, ConnectionCmd, SymbolPair, Symbols};

const GDAX_SYMBOLS_API: &'static str = "https://api.gdax.com/products";
const GDAX_WS: &'static str = "wss://ws-feed.gdax.com";
const EXCHANGE: &'static str = "gdax";

pub struct Gdax;

impl Named for Gdax {
    fn name() -> &'static str {
        EXCHANGE
    }
}

#[derive(Serialize, Deserialize)]
pub struct Tick {
    trade_id: i64,
    sequence: i64,
    time: String,
    product_id: String,
    price: String,
    side: String,
    last_size: String,
    best_bid: String,
    best_ask: String,
}

#[derive(Serialize, Deserialize)]
pub struct Level2 {
    product_id: String,
    changes: Vec<[String; 3]>
}

#[derive(Serialize, Deserialize)]
#[serde(tag = "type")]
#[serde(rename_all = "lowercase")]
pub enum GdaxData {
    Ticker(Tick),
    L2update(Level2),
}

fn parse_payload(s: String) -> Vec<(SymbolPair, GdaxData)> {

    if let Ok(parse) = serde_json::from_str(&s) {
        let symbol = match parse {
            GdaxData::Ticker(ref tick) => tick.product_id.clone(),
            GdaxData::L2update(ref level2) => level2.product_id.clone(),
        };
        vec![(SymbolPair::new(symbol), parse)]
    }
    else {
        Vec::new()
    }
}

fn unify_trade(t: Tick) -> Option<UnifiedFormat> {

   strict_match!(
     Some(side) = Side::try_from_str(t.side),
     Ok(price) = f64::from_str(t.price.as_str()),
     Ok(amount) = f64::from_str(t.last_size.as_str()), {
       UnifiedFormat{
         exchange: EXCHANGE.to_string(),
         symbol: "".to_string(),
         trades: vec![
           TradeData{
               id: t.product_id, 
               timestamp: 0,
               datetime: "".to_string(),
               side: side,
               price: price,
               amount: amount
           }
         ],
         depth: None,
       }
     })
}

fn unify_book(d: Level2) -> UnifiedFormat {
   let mut buys: Vec<&[String; 3]> = Vec::new();
   let mut sells: Vec<&[String; 3]> = Vec::new();

   for change in d.changes.iter() {
     match change[0].as_str() {
         "buy" => buys.push(change),
         "sell" => sells.push(change),
         _ => (),
     }
   }

   let normed_buys = buys.iter().filter_map(|b: &&[String; 3]| DepthPoint::from_strings(&b[1], &b[2])).collect();
   let normed_sells = sells.iter().filter_map(|b: &&[String; 3]| DepthPoint::from_strings(&b[1], &b[2])).collect(); 

   UnifiedFormat{
       exchange: EXCHANGE.to_string(),
       symbol: "".to_string(),
       trades: Vec::new(),
       depth: Some(DepthData::new(normed_buys, normed_sells))
   }
}

impl FetchSymbols for Gdax {
    fn fetch_symbols() -> Symbols {
        let exchange_info = reqwest::get(GDAX_SYMBOLS_API).unwrap().text().unwrap();
    let info_json: Value = serde_json::from_str(&exchange_info).unwrap();
    Symbols(info_json
        .as_array()
        .unwrap()
        .clone()
        .iter()
        .map(|s| s["id"].as_str().unwrap().to_string())
        .collect())
    }

}

pub type GdaxStream = Tagged<Gdax, Api<Client<TcpStream>, ()>>;

impl ApiStream for GdaxStream {
    type Payload = GdaxData;

    fn format_symbols(s: &String) -> SymbolPair {
        SymbolPair::new(url_symbols::replace_pipe(s))
    }

    fn mk_connection(symbols: &Vec<String>) -> Vec<Connection> {
        let message = json!({
            "type": "subscribe",
            "product_ids": symbols, 
            "channels": [ "level2", "ticker" ]
        }).to_string();
        vec![ Connection {
            uri: GDAX_WS.to_string(),
            lifetime: Duration::new(10800,0),
            delay: Duration::new(0,0),
            instructions: vec![ ConnectionCmd::Send(message) ]
        }]
    }

    fn parse_payload(&self, s: String) -> Vec<(SymbolPair, Self::Payload)> {
        parse_payload(s)
    }

    fn unify(payload: Self::Payload) -> Option<UnifiedFormat> {
        match payload {
            GdaxData::Ticker(t) => unify_trade(t),
            GdaxData::L2update(d) => Some(unify_book(d)),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_json::from_reader;
    use std::fs::File;

    fn with_mock_data<T> (test: T)
    where T: FnOnce(Value) {
        match File::open("assets/mock.json") {
            Ok(file) => match from_reader(file) {
                Ok(data) => test(data),
                Err(e) => panic!("error parsing mock.json: {}", e),
            }
            Err(e) => panic!("error opening mock.json: {}", e),
        }
    }

    #[test]
    fn gdax_parse_book_payload() {
        with_mock_data(|data| {
            let payload = &data["gdax_book"];
            assert_eq!(parse_payload(payload.to_string()).len(), 1)
        })
    }

    #[test]
    fn gdax_parse_trade_payload() {
        with_mock_data(|data| {
            let payload = &data["gdax_trade"];
            assert_eq!(parse_payload(payload.to_string()).len(), 1)
        })
    }
}
