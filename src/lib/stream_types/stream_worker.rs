use slog::Logger;
use std::sync::{Arc, Condvar, Mutex, RwLock};
use std::sync::mpsc::Sender;
use std::time::Duration;
use websocket::result::WebSocketError;

use assignment::Assignment;
use stream_types::connection::{Connection, ServerEvent};
use stream_types::stream::*;
use worker::Worker;

pub struct StreamWorker<T> {
  assignment: T,
  name: String,
  connection: Connection,
  stream: Option<Stream>,
  push_data: Sender<ServerEvent>,
  assignment_sync: Arc<(Mutex<T>, Condvar)>,
  new_thread_ready: Arc<RwLock<Option<bool>>>,
  log: Logger,
}

pub enum StreamWorkerError {
  CouldNotStartStream,
}

impl<T> StreamWorker<T> {
  pub fn new(
    name: String,
    log: Logger,
    connection: Connection,
    assignment: T,
    push_data: Sender<ServerEvent>,
    assignment_sync: Arc<(Mutex<T>, Condvar)>,
    new_thread_ready: Arc<RwLock<Option<bool>>>,
  ) -> StreamWorker<T> {
    StreamWorker {
      assignment: assignment,
      name: name,
      connection: connection,
      stream: None,
      push_data: push_data,
      assignment_sync: assignment_sync,
      new_thread_ready: new_thread_ready,
      log: log,
    }
  }
}

impl<T> Worker for StreamWorker<T>
where
  T: Assignment,
{
  type Error = StreamWorkerError;

  fn wait_for_assignment(&self) {
    let &(ref lock, ref cvar) = &*self.assignment_sync;

    // Wait for thread assignment to switch
    if let Ok(mut condition) = lock.lock() {
      while *condition != self.assignment {
        if let Ok(new_condition) = cvar.wait(condition) {
          condition = new_condition;
        } else {
          return ();
        }
      }
    }
  }

  fn lifetime(&self) -> &Duration {
    &self.connection.lifetime
  }

  fn start_work_delay(&self) -> &Duration {
    &self.connection.delay
  }

  fn signal_not_ready_for_work(&self) {
    if let Ok(mut thread_ready) = self.new_thread_ready.write() {
      *thread_ready = Some(false);
    }
  }

  fn setup_for_work(&mut self) -> Result<(), Self::Error> {
    if let Some(stream) = setup_connection(self.connection.clone()).ok() {
      self.stream = Some(stream);
      Ok(())
    } else {
      Err(StreamWorkerError::CouldNotStartStream)
    }
  }

  fn signal_ready_for_work(&self) {
    if let Ok(mut thread_ready) = self.new_thread_ready.write() {
      *thread_ready = Some(true);
    }
  }

  fn signal_ready_for_replacement(&self) {
    let &(ref lock, ref cvar) = &*self.assignment_sync;
    if let Ok(mut condition) = lock.lock() {
      *condition = self.assignment.next_assignment();
      cvar.notify_one();
    }
  }

  fn work_until<F>(&mut self, stopping_condition: F)
  where
    F: Fn() -> bool,
  {
    if let Some(ref mut stream) = self.stream {
      loop {
        match stream.0.recv_message() {
          Ok(item) => {
            let _ = self.push_data.send(ServerEvent::Broadcast(
              (self.name.clone(), item),
            ));
            if stopping_condition() {
              break;
            }
          }
          Err(WebSocketError::NoDataAvailable) => break,
          _ => (),
        }
      }
    }
  }

  fn work_until_replacement_ready(&mut self) {
    if let Ok(mut thread_ready) = self.new_thread_ready.write() {
      *thread_ready = None;
    }
    if let Ok(thread_ready) = self.new_thread_ready.read() {
      if let Some(ref mut stream) = self.stream {
        loop {
          match stream.0.recv_message() {
            Ok(item) => {
              let _ = self.push_data.send(ServerEvent::Broadcast(
                (self.name.clone(), item),
              ));
              if let Some(true) = *thread_ready {
                break;
              }
            }
            Err(WebSocketError::NoDataAvailable) => break,
            _ => (),
          }
        }
      }
    }
  }

  fn on_work_setup_fail(&self) {
    crit!(self.log, "Unable to restart connection";
                          "detail" => format!("{}", self.name.clone()));
  }
}
