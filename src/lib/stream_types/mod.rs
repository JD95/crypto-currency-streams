pub mod stream;
pub mod stream_worker;
pub mod connection;

use slog::Logger;
use std::fmt;
use std::sync::{Arc, Condvar, Mutex, RwLock};
use std::sync::mpsc::Sender;
use std::thread;
use std::thread::JoinHandle;
use std::time::Instant;

pub use self::connection::{Connection, ConnectionCmd, ServerEvent};
pub use self::stream_worker::*;
use worker::Worker;

pub struct Symbols(pub Vec<String>);

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct SymbolPair(String);

impl SymbolPair {
  pub fn new(s: String) -> SymbolPair {
    SymbolPair(s)
  }
}

impl fmt::Display for SymbolPair {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", self.0)
  }
}

pub struct RestartingStream {
  thread_a: JoinHandle<()>,
  thread_b: JoinHandle<()>,
}

pub struct ThreadLane(bool);

fn launch_restarting_thread<T>(mut worker: T) -> JoinHandle<()>
where
  T: Worker + Send + 'static,
{
  thread::spawn(move || loop {
    worker.wait_for_assignment();
    worker.signal_not_ready_for_work();

    if let Ok(_) = worker.setup_for_work() {
      thread::sleep(*worker.start_work_delay());
      worker.signal_ready_for_work();

      let reset_timer = Instant::now();
      let lifetime = worker.lifetime().clone();

      worker.work_until(|| reset_timer.elapsed() > lifetime);
      worker.signal_ready_for_replacement();
      worker.work_until_replacement_ready();

    } else {
      worker.on_work_setup_fail();
    }
  })
}

impl RestartingStream {
  pub fn new(
    name: String,
    log: Logger,
    connection: Connection,
    push_data: Sender<ServerEvent>,
  ) -> RestartingStream {
    let assignment_sync = Arc::new((Mutex::new(true), Condvar::new()));
    let new_thread_ready = Arc::new(RwLock::new(None));

    let thread_a = launch_restarting_thread(StreamWorker::new(
      name.clone(),
      log.clone(),
      connection.clone(),
      true,
      push_data.clone(),
      assignment_sync.clone(),
      new_thread_ready.clone(),
    ));

    let thread_b = launch_restarting_thread(StreamWorker::new(
      name.clone(),
      log.clone(),
      connection.clone(),
      false,
      push_data.clone(),
      assignment_sync.clone(),
      new_thread_ready.clone(),
    ));

    RestartingStream {
      thread_a: thread_a,
      thread_b: thread_b,
    }
  }
}
