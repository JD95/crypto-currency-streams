use std::time::Duration;
use websocket::message::OwnedMessage;
use websocket::server::upgrade::WsUpgrade;
use websocket::server::upgrade::sync::Buffer;
use websocket::stream::sync::TcpStream;

#[derive(Clone)]
pub enum ConnectionCmd {
  Send(String),
}

#[derive(Clone)]
pub struct Connection {
  pub uri: String,
  pub lifetime: Duration,
  pub delay: Duration,
  pub instructions: Vec<ConnectionCmd>,
}

pub enum ServerEvent {
  Broadcast((String, OwnedMessage)),
  Subscribe(WsUpgrade<TcpStream, Option<Buffer>>),
}
