use native_tls::TlsConnector;
use std::time::Duration;
use websocket::client::ClientBuilder;
use websocket::client::sync::Client;
use websocket::message::OwnedMessage;
use websocket::stream::sync::{TcpStream, TlsStream};

use stream_types::{Connection, ConnectionCmd};

pub struct Stream(pub Client<TlsStream<TcpStream>>);

pub fn setup_connection(connection: Connection) -> Result<Stream, String> {
  let connector = TlsConnector::builder().unwrap().build().unwrap();
  ClientBuilder::new(connection.uri.as_str())
    .map_err(|e| e.to_string())
    .and_then(|mut c| {
      c.connect_secure(Some(connector))
        .map_err(|e| e.to_string())
        .and_then(move |mut stream| {
          for cmd in connection.instructions {
            match cmd {
              ConnectionCmd::Send(m) => {
                let _ = stream.send_message(&OwnedMessage::Text(m));
              }
            }
          }

          {
            let stream = stream.stream_ref().get_ref();
            let _ = stream.set_read_timeout(Some(Duration::new(1, 0)));
            let _ = stream.set_write_timeout(Some(Duration::new(1, 0)));
          }

          Ok(Stream(stream))
        })
    })
}
