pub trait Assignment: Eq {
  fn next_assignment(&self) -> Self;
}

impl Assignment for bool {
  fn next_assignment(&self) -> bool {
    !self
  }
}
