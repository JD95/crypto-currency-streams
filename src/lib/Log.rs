use slog;
use std::fmt::Display;

pub trait Log {
    fn info<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>);
    fn error<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>);
    fn crit<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>);
    fn warn<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>);
    fn trace<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>);
    fn debug<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>);
}

impl Log for slog::Logger {
    fn info<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>) {
        match details {
            Some(d) => info!(self, title, message, format!("{}", d),
            None => info!(self, title, message),
        }
    }
    fn error<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>) {
        match details {
            Some(d) => error!(self, title, message, format!("{}", d),
            None => error!(self, title, message),
        }
    }
    fn crit<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>) {
        match details {
            Some(d) => crit!(self, title, message, format!("{}", d),
            None => crit!(self, title, message),
        }
    }
    fn warn<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>) {
        match details {
            Some(d) => warn!(self, title, message, format!("{}", d),
            None => warn!(self, title, message),
        }
    }
    fn trace<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>) {
        match details {
            Some(d) => trace!(self, title, message, format!("{}", d),
            None => trace!(self, title, message),
        }
    }
    fn debug<T: Display>(&mut self, title: &'static str, message: &'static str, details: Option<T>) {
        match details {
            Some(d) => debug!(self, title, message, format!("{}", d),
            None => debug!(self, title, message),
        }
    }

}
