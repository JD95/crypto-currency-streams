pub struct BlockList<T> {
    block: Vec<T>,
}

impl<T> BlockList<T> {
    pub fn insert(&mut self, x: T) {
        self.block.push(x);
    }

    pub fn len(&self) -> usize {
        self.block.len()
    }

    pub fn map<F>(&mut self, func: F)
    where
        F: Fn(&mut T) -> bool,
    {

        let mut n = self.block.len();
        let mut i = 0;
        while i < n {
            loop {
                let success = self.block.get_mut(i).map(|slot| func(slot)).unwrap_or(
                    false,
                );
                if !success && n > 0 && i < n {
                    self.block.swap_remove(i);
                    n -= 1;
                } else {
                    break;
                }
            }
            i += 1;
        }
    }

    pub fn new(n: usize) -> BlockList<T> {
        BlockList { block: Vec::with_capacity(n) }
    }
}

#[test]
fn elem_removed_on_fail() {
    let mut list = BlockList::new(3);
    list.insert(true);
    list.insert(false);
    list.insert(true);

    list.map(|x| *x);
    assert_eq!(list.len(), 2);
}

#[test]
fn map_works_on_empty() {
    let mut list = BlockList::new(3);
    list.insert(false);
    list.map(|x| *x);
    list.map(|x| *x);
    assert!(true);
}
