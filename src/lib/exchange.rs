use slog;
use std::collections::HashMap;
use websocket::message::OwnedMessage;
use websocket::stream::sync::TcpStream;
use websocket::sync::Client;

use api_stream::ApiStream;
use block_list::*;
use json::UnifiedFormat;
use stream_types::SymbolPair;
use symbol_channel::SymbolChannel;
use util::{ModifyLookup, Named};

pub trait Exchange {
  fn push_payload(&self, &slog::Logger, String) -> Result<(), String>;
  fn subscribe(&self, &slog::Logger, Client<TcpStream>, String) -> Result<(), String>;
}

pub type Exchanges = HashMap<String, Box<Exchange + Sync + Send>>;

fn send_data(message: &OwnedMessage, sub: &mut Client<TcpStream>) -> bool {

  if !sub.send_message(message).is_ok() {
    let _ = sub.shutdown();
    false
  } else {
    true
  }
}

fn add_to_snapshot<ClientT>(channel: &mut SymbolChannel<ClientT>, unified: &UnifiedFormat) {
  for ref item in &unified.trades {
    channel.push_trade((*item).clone());
  }
  if let Some(ref depth) = unified.depth {
    for ref item in &depth.asks {
      channel.push_ask((*item).clone());
    }
    for ref item in &depth.bids {
      channel.push_bid((*item).clone());
    }
  }
}

impl<A> Exchange for A
where
  A: ApiStream + Named + ModifyLookup<SymbolPair, SymbolChannel<Client<TcpStream>>>,
{
  fn push_payload(&self, log: &slog::Logger, payload: String) -> Result<(), String> {
    use serde_json::ser::*;

    for (sym, p) in self.parse_payload(payload) {
      if let Some(unified) = A::unify(p) {
        if let Ok(m) = to_string(&unified) {
          let message = OwnedMessage::Text(m);
          let f = |channel: &mut SymbolChannel<Client<TcpStream>>| {
            add_to_snapshot(channel, &unified);
            channel.subscriptions.map(|s| send_data(&message, s));
          };
          if !self.lookup_and_modify(&sym, f) {
            error!(log, "bad-symbol";
                            "error" => "Attempted to push to non-existant symbol!",
                            "detail" => format!("{}", sym))
          }
        }
      }
    }
    Ok(())
  }

  fn subscribe(
    &self,
    _log: &slog::Logger,
    mut client: Client<TcpStream>,
    sym: String,
  ) -> Result<(), String> {
    use serde_json::ser::*;

    if !self.lookup_and_modify(&Self::format_symbols(&sym), |channel| {
        if let Ok(message) = to_string(&channel.use_snapshot(A::name(), &sym)) {
            send_data(&OwnedMessage::Text(message), &mut client);
        }
      channel.subscriptions.insert(client)
    })
    {
      let message = "Client attemtped to subscribe to non exsistant symbol stream ";
      Err(message.to_string() + sym.as_str())
    } else {
      Ok(())
    }
  }
}
