use std::time::Duration;

pub trait Worker {
  type Error;
  fn wait_for_assignment(&self);
  fn lifetime(&self) -> &Duration;
  fn start_work_delay(&self) -> &Duration;
  fn signal_not_ready_for_work(&self);
  fn setup_for_work(&mut self) -> Result<(), Self::Error>;
  fn signal_ready_for_work(&self);
  fn signal_ready_for_replacement(&self);
  fn work_until<F>(&mut self, stopping_condition: F)
  where
    F: Fn() -> bool;
  fn work_until_replacement_ready(&mut self);
  fn on_work_setup_fail(&self);
}
