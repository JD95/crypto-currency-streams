use std::collections::{HashMap, VecDeque};
use std::sync::{Arc, Mutex};

use block_list::*;
use json::{DepthData, DepthPoint, TradeData, UnifiedFormat};
use stream_types::SymbolPair;
use util::{ModifyLookup, New};

const MAX_TRADES: usize = 50;
const MAX_BOOKS: usize = 50;

pub struct SymbolChannel<ClientT> {
  pub subscriptions: BlockList<ClientT>,
  pub trades_snapshot: VecDeque<TradeData>,
  pub asks_snapshot: VecDeque<DepthPoint>,
  pub bids_snapshot: VecDeque<DepthPoint>,
}

fn push_item<T>(snapshot: &mut VecDeque<T>, max: &'static usize, item: T) {
  if snapshot.len() > *max {
    let _ = snapshot.pop_back();
  }
  snapshot.push_front(item);
}

impl<ClientT> SymbolChannel<ClientT> {
  pub fn push_trade(&mut self, trade: TradeData) {
    push_item(&mut self.trades_snapshot, &MAX_TRADES, trade);
  }

  pub fn push_ask(&mut self, ask: DepthPoint) {
    push_item(&mut self.asks_snapshot, &MAX_BOOKS, ask);
  }

  pub fn push_bid(&mut self, bid: DepthPoint) {
    push_item(&mut self.bids_snapshot, &MAX_BOOKS, bid);
  }

  pub fn use_snapshot(&self, exchange: &'static str, symbol: &String) -> UnifiedFormat {
    UnifiedFormat {
      exchange: exchange.to_string(),
      symbol: symbol.clone(),
      trades: self.trades_snapshot.iter().cloned().collect(),
      depth: Some(DepthData::new(
        self.asks_snapshot.iter().cloned().collect(),
        self.bids_snapshot.iter().cloned().collect(),
      )),
    }
  }
}

impl<ClientT> New for SymbolChannel<ClientT> {
  fn new() -> SymbolChannel<ClientT> {
    SymbolChannel {
      subscriptions: BlockList::new(1000),
      trades_snapshot: VecDeque::new(),
      asks_snapshot: VecDeque::new(),
      bids_snapshot: VecDeque::new(),
    }
  }
}

pub struct SymbolChannels<ClientT> {
  pub channels: HashMap<SymbolPair, Arc<Mutex<SymbolChannel<ClientT>>>>,
}

impl<ClientT> New for SymbolChannels<ClientT> {
  fn new() -> SymbolChannels<ClientT> {
    SymbolChannels {
      channels: HashMap::new(),
    }
  }
}

impl<ClientT> ModifyLookup<SymbolPair, SymbolChannel<ClientT>> for SymbolChannels<ClientT> {
  fn lookup_and_modify<F>(&self, key: &SymbolPair, f: F) -> bool
  where
    F: FnOnce(&mut SymbolChannel<ClientT>),
  {
    match self.channels.get(key) {
      Some(channel) => {
        let mut lock = channel.lock().unwrap();
        f(&mut lock);
        true
      }
      _ => false,
    }
  }
}
