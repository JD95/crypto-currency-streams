extern crate hyper;
extern crate native_tls;
extern crate num_cpus;
extern crate reqwest;
#[macro_use]
extern crate serde_json;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;
extern crate url;
extern crate websocket;

#[macro_use]
mod macros;

pub mod api;
pub mod api_stream;
pub mod assignment;
pub mod block_list;
pub mod exchange;
pub mod json;
pub mod stream_connections;
pub mod stream_types;
pub mod symbol_channel;
pub mod url_symbols;
pub mod util;
pub mod worker;

pub mod binance;
pub mod bitfinex;
pub mod gdax;

pub mod connect_client;
pub mod exchange_server;
