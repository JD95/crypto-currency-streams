use num_cpus;
use slog;
use std::collections::HashMap;
use std::sync::{Arc, Mutex, RwLock};
use std::sync::mpsc::channel;
use std::thread;
use websocket::message::OwnedMessage;
use websocket::sync::Server;

use binance::*;
use bitfinex::*;
use connect_client::*;
use exchange::Exchanges;
use gdax::*;
use stream_connections::*;
use stream_types::ServerEvent;
use stream_types::ServerEvent::*;

fn handle_server_event(log: &slog::Logger, exchanges: &Arc<RwLock<Exchanges>>, item: ServerEvent) {
  match item {
    Broadcast((exchange, OwnedMessage::Text(payload))) => {
      if let Ok(reader) = exchanges.read() {
        if let Some(ex) = reader.get(&exchange) {
          let _ = ex.push_payload(&log, payload);
        }
      }
    }
    Broadcast(_x) => (),
    Subscribe(upgrade) => {
      if let Ok(reader) = exchanges.read() {
        connect_client(&log, upgrade, reader);
      }
    }
  }
}

pub fn exchange_server(log: &slog::Logger, port: &str) {
  let server = Server::bind(port).unwrap();

  let (push_data, pull_data) = channel();

  {
    let push_data = push_data.clone();
    thread::spawn(move || for connection in server.filter_map(Result::ok) {
      let _ = push_data.send(Subscribe(connection));
    });
  }

  let mut streams = Vec::new();
  let exchanges: Arc<RwLock<Exchanges>> = {
    let mut map: Exchanges = HashMap::new();

    launch![
      streams,
      log,
      map,
      push_data,
      BinanceStream,
      GdaxStream,
      BitfinexStream,
    ];

    Arc::new(RwLock::new(map))
  };

  let receiver = Arc::new(Mutex::new(pull_data));
  let mut handles = Vec::new();

  for _i in 0..(num_cpus::get()) {
    let stream = Arc::clone(&receiver);
    let exchanges = Arc::clone(&exchanges);
    let log = log.clone();
    handles.push(thread::spawn(move || loop {
      if let Ok(items) = stream.lock() {
        if let Ok(item) = items.recv() {
          handle_server_event(&log, &exchanges, item);
        }
      }
    }));
  }

  for handle in handles {
    let _ = handle.join();
  }
}
