use json::UnifiedFormat;
use stream_types::{Connection, SymbolPair, Symbols};
use util::{Has, New};

pub trait ApiStream: Has<Symbols> + New {
  type Payload;
  fn format_symbols(&String) -> SymbolPair;
  fn mk_connection(&Vec<String>) -> Vec<Connection>;
  fn parse_payload(&self, String) -> Vec<(SymbolPair, Self::Payload)>;
  fn unify(Self::Payload) -> Option<UnifiedFormat>;
}

pub trait FetchSymbols {
  fn fetch_symbols() -> Symbols;
}
