use std::marker::PhantomData;
use std::sync::{Arc, Mutex};

use api_stream::FetchSymbols;
use block_list::*;
use stream_types::{SymbolPair, Symbols};
use symbol_channel::{SymbolChannel, SymbolChannels};
use util::{Has, ModifyLookup, Named, New, Tagged};

pub struct Api<ClientT, ExtensionT> {
  symbols: Symbols,
  subscriptions: SymbolChannels<ClientT>,
  pub extension: ExtensionT,
}

impl<ClientT, ExtensionT> Has<Symbols> for Api<ClientT, ExtensionT> {
  fn extract(&self) -> &Symbols {
    &self.symbols
  }
}

impl<Tag: FetchSymbols + Named, ClientT, ExtensionT: New> New
  for Tagged<Tag, Api<ClientT, ExtensionT>> {
  fn new() -> Tagged<Tag, Api<ClientT, ExtensionT>> {
    let symbols = Tag::fetch_symbols();
    let mut subscriptions = SymbolChannels::new();
    for s in &symbols.0 {
      subscriptions.channels.insert(
        SymbolPair::new(s.clone()),
        Arc::new(Mutex::new(SymbolChannel::new())),
      );
    }

    Tagged {
      value: Api {
        symbols: symbols,
        subscriptions: subscriptions,
        extension: ExtensionT::new(),
      },
      _tag: PhantomData,
    }
  }
}

impl<ClientT, ExtensionT> ModifyLookup<SymbolPair, SymbolChannel<ClientT>>
  for Api<ClientT, ExtensionT> {
  fn lookup_and_modify<F>(&self, key: &SymbolPair, f: F) -> bool
  where
    F: FnOnce(&mut SymbolChannel<ClientT>),
  {
    self.subscriptions.lookup_and_modify(key, f)
  }
}
