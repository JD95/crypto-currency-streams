use std::collections::HashMap;
use std::hash::Hash;
use std::marker::PhantomData;
use std::sync::{Arc, Mutex};

pub trait Has<T> {
  fn extract(&self) -> &T;
}

pub trait Modify<T> {
  fn modify<F>(&mut self, f: F)
  where
    F: FnMut(&mut T);
}

pub trait ModifyLookup<KeyT, ValueT> {
  fn lookup_and_modify<F>(&self, &KeyT, f: F) -> bool
  where
    F: FnOnce(&mut ValueT);
}

pub struct Name(pub String);

pub trait Named {
  fn name() -> &'static str;
}

pub struct Tagged<Tag, T> {
  pub value: T,
  pub _tag: PhantomData<Tag>,
}

impl<T, Tag, InnerT: Has<T>> Has<T> for Tagged<Tag, InnerT> {
  fn extract(&self) -> &T {
    self.value.extract()
  }
}

impl<T, Tag: Named> Named for Tagged<Tag, T> {
  fn name() -> &'static str {
    Tag::name()
  }
}

impl<KT, VT, T: ModifyLookup<KT, VT>, Tag> ModifyLookup<KT, VT> for Tagged<Tag, T> {
  fn lookup_and_modify<F>(&self, key: &KT, f: F) -> bool
  where
    F: FnOnce(&mut VT),
  {
    self.value.lookup_and_modify(key, f)
  }
}

pub trait New {
  fn new() -> Self;
}

impl New for () {
  fn new() -> () {
    ()
  }
}

impl<T: New> New for Arc<T> {
  fn new() -> Arc<T> {
    Arc::new(T::new())
  }
}

impl<T: New> New for Mutex<T> {
  fn new() -> Mutex<T> {
    Mutex::new(T::new())
  }
}

impl<K: Eq + Hash, T> New for HashMap<K, T> {
  fn new() -> HashMap<K, T> {
    HashMap::new()
  }
}
