use reqwest;
use serde_json::map::Map;
use serde_json::Value;
use serde_json;
use std::time::Duration;
use websocket::sync::Client;
use websocket::stream::sync::TcpStream;

use json::*;
use url_symbols;
use api::Api;
use api_stream::{ApiStream, FetchSymbols};
use stream_types::{Connection, SymbolPair, Symbols};
use util::{Tagged, Named};

pub const BINANCE_API: &'static str = "https://api.binance.com/api/v1/exchangeInfo";
const BINANCE_WS: &'static str = "wss://stream.binance.com:9443/stream?streams=";

const EXCHANGE: &'static str = "binance";

pub struct Binance;

impl Named for Binance {
    fn name() -> &'static str {
        EXCHANGE
    }
}

pub struct Tick;
pub struct Depth5;

pub enum BinanceData {
    Trade(serde_json::Value),
    Depth5(serde_json::Value),
}

fn deserialize_json_symbols(json_symbols: Vec<Value>) -> Vec<String> {
    let mut symbols = vec![];
    for symbol in json_symbols {
        match symbol["symbol"] {
            Value::String(ref s) => symbols.push(s.to_string().to_lowercase()),
            _ => println!("Error: Could not parse symbol"),
        }
    }
    symbols
}

fn fetch_binance_symbols() -> Vec<Value> {
    let exchange_info = reqwest::get(BINANCE_API).unwrap().text().unwrap();
    let info_json: Value = serde_json::from_str(&exchange_info).unwrap();
    info_json["symbols"].as_array().unwrap().clone()
}

fn symbol_stream(symbols: &Vec<String>, stream: &str) -> String {
    [&symbols.join(&(stream.to_string() + "/")), stream].join("")
}

fn binance_parse_payload(s: String) -> Vec<(SymbolPair, BinanceData)> {
    use serde_json::Value;
    use serde_json::Value::Object;
    use self::BinanceData::*;

    let parse: Option<(String, Value)> = match serde_json::from_str(&s) {
        Ok(Object(mut obj)) => {
            strict_match!(
            Some(data) = obj.remove("data"),
            Some(Value::String(stream)) = obj.remove("stream"),{
                (stream, data)
        })
        }
        _ => None,
    };
    parse
        .and_then(|(stream, data)| {
            let info: Vec<String> = stream.split('@').map(|s| s.to_string()).collect();
            strict_match!(Some(pair) = info.get(0), Some(channel) = info.get(1), {
                let symbol = SymbolPair::new(pair.clone());
                match channel.as_str() {
                    "depth5" => vec![(symbol, Depth5(data))],
                    "trade" => vec![(symbol, Trade(data))],
                    _ => Vec::new(),
                }
            })
        })
        .unwrap_or(Vec::new())
}


fn unify_binace_book(book: Value) -> Option<UnifiedFormat> {
    let f = |xs: &Vec<Value>| (xs[0].clone(), xs[1].clone());
    let fields =
        strict_match!(
        Some(v_asks) = book.get("asks"),
        Some(v_bids) = book.get("bids"),
        {
            (v_asks, v_bids)
        }
    );
    fields.and_then(|fs| {
        strict_match!(
        Some(asks) = norm_depth(&fs.0, &f),
        Some(bids) = norm_depth(&fs.1, &f), {
        UnifiedFormat{
           exchange: EXCHANGE.to_string(),
           symbol: "".to_string(),
           trades: Vec::new(),
           depth: Some(DepthData{asks: asks, bids: bids})
        }
        })
    })
}

fn unify_binance_trade(mut trade: Map<String, Value>) -> Option<UnifiedFormat> {
    let fields =
        strict_match!(
        Some(v_timestamp) = trade.remove("T").and_then(|t| t.as_i64()),
        Some(v_side) = trade.remove("m"),
        Some(v_price) = trade.remove("p"),
        Some(v_amount) = trade.remove("q"), {
            (v_timestamp, v_side, v_price, v_amount)
        });
    fields.and_then(|fs| {
        strict_match!(
        Some(id) = trade.remove("t").and_then(|id| id.as_i64()),
        Some(side) = json_side(&fs.1),
        Some(price) =json_str_cast::<f64>(&fs.2),
        Some(amount) = json_str_cast::<f64>(&fs.3), {
            let timestamp = fs.0;
            UnifiedFormat{
                exchange: EXCHANGE.to_string(),
                symbol: "".to_string(),
                trades: vec![
                    TradeData{
                        id: id.to_string(),
                        timestamp: timestamp,
                        datetime: "".to_string(),
                        side: side, 
                        price: price, 
                        amount: amount 
                    }
                ],
                depth: None, 
            }
        })
    })
}

fn construct_uri(symbols: &Vec<String>) -> String {
        let depth5_streams = symbol_stream(&symbols, "@depth5");
        let trade_streams = symbol_stream(&symbols, "@trade");
        [BINANCE_WS, &depth5_streams, "/", &trade_streams].join("")
}

impl FetchSymbols for Binance {
    fn fetch_symbols() -> Symbols {
        let symbols_json =  fetch_binance_symbols();
        Symbols(deserialize_json_symbols(symbols_json))
    }
}

pub type BinanceStream = Tagged<Binance, Api<Client<TcpStream>, ()>>;

impl ApiStream for BinanceStream  {
    type Payload = BinanceData;

    fn format_symbols(s: &String) -> SymbolPair{
        SymbolPair::new(url_symbols::symbol_lowercase(s))
    }

    fn mk_connection(symbols: &Vec<String>) -> Vec<Connection> {
        let lane_a = symbols[1..50].to_vec();
        let lane_b = symbols[50..].to_vec();
        vec![
            Connection {
                uri: construct_uri(&lane_a),
                lifetime: Duration::new(10800, 0),
                delay: Duration::new(0,0),
                instructions: Vec::new()
            },
            Connection {
                uri: construct_uri(&lane_b),
                lifetime: Duration::new(10800, 0),
                delay: Duration::new(0,0),
                instructions: Vec::new()
            },
        ]
    }

    fn parse_payload(&self, s: String) -> Vec<(SymbolPair, BinanceData)> {
        binance_parse_payload(s)
    }

    fn unify(payload: BinanceData) -> Option<UnifiedFormat> {
        use serde_json::Value::*;

        match payload {
            BinanceData::Depth5(d) => unify_binace_book(d),
            BinanceData::Trade(Object(t)) => unify_binance_trade(t), 
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_json::from_reader;
    use std::fs::File;

    fn with_mock_data<T> (test: T)
    where T: FnOnce(Value) {
        match File::open("assets/mock.json") {
            Ok(file) => match from_reader(file) {
                Ok(data) => test(data),
                Err(e) => panic!("error parsing mock.json: {}", e),
            }
            Err(e) => panic!("error opening mock.json: {}", e),
        }
    }

    #[test]
    fn binance_parse_book_payload() {
        with_mock_data(|data| {
            let payload = &data["binance_parse_book_payload"];
            assert_eq!(binance_parse_payload(payload.to_string()).len(), 1)
        })
    }

    #[test]
    fn binance_parse_trade_payload() {
        with_mock_data(|data| {
            let payload = &data["binance_parse_trade_payload"];
            assert_eq!(binance_parse_payload(payload.to_string()).len(), 1)
        })
    }

    #[test]
    fn binance_unify_book() {
        with_mock_data(|data| {
            let payload = &data["binance_unify_book"];
            assert!(unify_binace_book(payload.clone()).is_some())
        })
    }

    #[test]
    fn binance_unify_trade() {
        with_mock_data(|data| {
            let payload = &data["binance_unify_trade"];
            assert!(unify_binance_trade(payload.as_object().unwrap().clone()).is_some())
        })
    }

}
