#[macro_export]
macro_rules! strict_match {
    ( $($matches:pat = $args:expr),+ , $f:block ) => {
        match ($($args),+) {
            ($($matches),+) => Some({ $f }),
            _ => None,
        }
    };
}


#[macro_export]
macro_rules! launch {
    ( $streams:expr, $log:expr, $exchanges:expr, $push_data:expr, ) => { };
    ( $streams:expr, $log:expr, $exchanges:expr, $push_data:expr, $x:tt) => {
        {
           $streams.append(&mut launch_data_stream::<$x>($log.clone(), &mut $exchanges, $push_data.clone()));
        }
    };
    ( $streams:expr, $log:expr, $exchanges:expr, $push_data:expr, $x:tt, $($y:tt)*) => {
        launch![$streams, $log, $exchanges, $push_data, $x];
        launch![$streams, $log, $exchanges, $push_data, $($y)*];
    };
}
