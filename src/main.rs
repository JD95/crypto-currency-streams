extern crate demeter;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;

use slog::Drain;
use std::fs::OpenOptions;
use std::thread;

use demeter::exchange_server::*;

fn main() {

  loop {
    let child = thread::spawn(|| {
      let log_path = "events.log";
      let file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(false)
        .open(log_path)
        .unwrap();

      let drain = slog_async::Async::default(
        slog_term::CompactFormat::new(slog_term::PlainSyncDecorator::new(file))
          .build()
          .fuse(),
      );
      let logger = slog::Logger::root(drain.fuse(), o!("build" => "1", "version" => "0.0.2"));
      exchange_server(&logger, "0.0.0.0:4040");

    });
    let _ = child.join();
  }
}
